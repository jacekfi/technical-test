# Docs to Technical Test

## Install Vagrant
Download and install Vagrant package dedicated for your OS and architecture from https://www.vagrantup.com/downloads.html

## Install VirtualBox
Download and install VirtualBox package dedicated for your OS and architecture from https://www.virtualbox.org/wiki/Downloads

## Clone Git Repository with the Solution
Clone remote repository to your local machine
```
$ git clone https://bitbucket.org/jacekfi/technical-test.git
```

## Start up Vagrant Box
From cloned repository root directory start up your Vagrant box by executing below command  
```
$ vagrant up
```
Note: Base Vagrant box used in this project is centos/7

## URL to Static Web Page
To access web page from web browser please enter below URL
```
http://127.0.0.1:8081
```

## Destroy Vagrant Box
To stop and destroy your Vagrant box please execute
```
$ vagrant destroy
```

