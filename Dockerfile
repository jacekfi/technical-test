FROM library/alpine:latest

RUN apk update \
    && apk add nginx 

WORKDIR /var/www

COPY index.html .

COPY default.conf /etc/nginx/conf.d/

RUN mkdir -p /run/nginx/

RUN chmod -R 0755 /var/www

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]